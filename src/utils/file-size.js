export default function formatBytes(bytes, decimals = 2) {
	if (bytes === 0) return '0 Bytes'

	const k = 1024
	const dm = decimals < 0 ? 0 : decimals
	const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']

	const i = Math.floor(Math.log(bytes) / Math.log(k))

	return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i]
}

export function getPages(file) {
	// console.log(file)
	var reader = new FileReader()
	reader.readAsBinaryString(file.blob)
	var count
	reader.onloadend = function () {
		count = reader.result.match(/\/Type[\s]*\/Page[^s]/g).length
		// console.log('Number of Pages:', count)
		return count
	}

	// console.log(count)
	// return count
}
